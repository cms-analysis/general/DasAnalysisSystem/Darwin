#ifndef DARWIN_EVENT_H
#define DARWIN_EVENT_H

#include <iostream>
#include <vector>
#include <Math/PtEtaPhiM4D.h>
#include <Math/Cylindrical3D.h>
#include <Math/Polar2D.h>
#include <TRandom3.h>

namespace Darwin::Physics {

////////////////////////////////////////////////////////////////////////////////
/// Abstract structure for events in data and MC
struct Event {
    std::vector<float> weights;
    virtual float weight () const = 0; //!< access to weight, using `iWgt` in daughter classes
    virtual void clear (); //!< resets all members to the same values as in the constructor
    Event ();
    virtual ~Event () = default;
};

////////////////////////////////////////////////////////////////////////////////
/// Structure for events in MC only with generator information
struct GenEvent : public Event {
    static size_t iWgt; //!< global index for weight
    float weight () const final; //!< uses `iWgt` internally
    int process; //!< flag for process (TODO?)
    float hard_scale; //!< hard scale, corresponding to `pthat` in Pythia 8
    void clear () override; //!< resets all members to the same values as in the constructor
    GenEvent ();
};

////////////////////////////////////////////////////////////////////////////////
/// Structure for events in data and MC
/// 
/// A priori, in MC, the values for the fill and run numbers are trivial.
/// It is however in principle possible to distribute the MC events in a 
/// realistic way to better account for time dependence.
struct RecEvent : public Event {
    static size_t iWgt; //!< global index for weight
    float weight () const final; //!< uses `iWgt` internally
    int fill, //!< 4-digit LHC fill
        runNo, //!< 6-digit run number
        lumi;  //!< lumi section
    unsigned long long evtNo; //!< event number
    void clear () override; //!< resets all members to the same values as in the constructor
    RecEvent ();
};

////////////////////////////////////////////////////////////////////////////////
/// Structure for triggers in data 
/// 
/// Triggers aren't always simulated, therefore this structure is a priori only
/// found in data n-tuples. It includes the necessary information to determine
/// the right trigger to normalise an event.
struct Trigger {
    std::vector<bool> Bit; //!< indicates which trigger has fired
    std::vector<int> PreHLT,   //!< HLT prescale
                     PreL1min, //!< L1 min pre-scale
                     PreL1max; //!< L1 max pre-scale

    void clear (); //!< resets all members to the same values as in the constructor
};

////////////////////////////////////////////////////////////////////////////////
/// Abstract structure for MET information
struct MET : public ROOT::Math::Polar2D<float> {
    float SumEt; //!< scalar sum of the transverse momentum of visible object
    virtual void clear (); //!< to clear for each new event in *n*-tupliser
    MET ();
    virtual ~MET () = default;
};

//struct GenMET : public MET {
//	// TODO
//    void clear () override; //!< resets all members to the same values as in the constructor
//};

////////////////////////////////////////////////////////////////////////////////
/// Structure for MET information at detector level
///
/// Not only the MET information itself but also the MET filters (CMS jargon for
/// event filters).
struct RecMET : public MET {
	// TODO: both corr and uncorr?
    std::vector<bool> Bit; //!< flags for application of MET filters (see *n*-tupliser config file)
    void clear () override; //!< resets all members to the same values as in the constructor
};

////////////////////////////////////////////////////////////////////////////////
/// Contains all relevant information to apply PU staub sauger, profile
/// reweighting, and JER (re)smearing
///
/// [TWiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/PileupJSONFileforData)
struct PileUp {

    static float MBxsec, //!< minimum-bias cross section
                 MBxsecRelUnc; //!< relative uncertainty on minimum-bias cross section

    float rho; //!< soft activity (see formula 7.15 in Patrick's thesis)
    int nVtx; //!< number of vertices in the event
    float trpu; //!< true pile-up
    int intpu; //!< in-time pile-up (i.e. from the same bunch crossing)
    float pthatMax; //!< hard-scale of the hardest pile-up event (to deal with anomalous events)

    float GetTrPU (const char = '0') const; //!< true pile-up variations
    float GetIntPU (const char = '0') const; //!< in-time pile-up variations
    float GetIntPU (TRandom3&, const char = '0') const; //!< estimate in-time pileup on the fly from the true pileup (useful in MC to generate a profile)
    void clear (); //!< resets all members to the same values as in the constructor

    PileUp ();
};

////////////////////////////////////////////////////////////////////////////////
/// Vertex information for CMS standard quality selection
struct Vertex : public ROOT::Math::Cylindrical3D<float> {
    float chi2; //!< figure of merit of the vertex fit
    int ndf; //!< number of degrees of freedom in vertex fit
    bool fake; //!< flag for fake vertices 

    void clear (); //!< resets all members to the same values as in the constructor
    Vertex ();
};

////////////////////////////////////////////////////////////////////////////////
/// An extended version of ROOT 4-vectors
struct FourVector : public ROOT::Math::PtEtaPhiM4D<float> {

    virtual float Rapidity () const; //!< return rapidity 
    virtual float AbsRap () const; //!< return absolute rapidity 

    virtual void clear (); //!< resets all members to the same values as in the constructor

    virtual FourVector& operator+= (const ROOT::Math::PtEtaPhiM4D<float>&);
    virtual FourVector operator+ (const ROOT::Math::PtEtaPhiM4D<float>&) const;

    FourVector () = default;
    FourVector (const ROOT::Math::PtEtaPhiM4D<float>&);
    virtual ~FourVector () = default;
};

////////////////////////////////////////////////////////////////////////////////
/// An abstract class for jets
struct Jet : public FourVector {

	static float R; //!< parameter of clustering algorithm

    char pdgID, //!< pdgID (default is 0 = undefined)
         nBHadrons, //!< number of *B* hadrons (0, 1 or 2+)
         nCHadrons; //!< number of *C* hadrons (0, 1+ in 2017; 0, 1, 2+ in 2016)

    std::vector<float> scales, //!< variations of the energy scale (e.g. JES or JER), a priori only for rec-level,
                               //!< but the possibility for this variable to be useful at gen level shouldn't be excluded
                       weights; //!< jet weight (typically expected to be trivial for all-flavour inclusive jet cross section)

    virtual float CorrPt  () const = 0; //!< uses the scale internally with global index in daughter class
    virtual float CorrEta () const = 0; //!< uses the scale internally with global index in daughter class
    // TODO: mass??

    virtual Jet& operator+= (const Jet&);

    virtual float weight () const = 0; //!< access to weight, using `iWgt` in daughter classes
    void clear () override; //!< resets all members to the same values as in the constructor

    Jet (); //!< constructor
    virtual ~Jet () = default; //!< destructor
};

////////////////////////////////////////////////////////////////////////////////
/// A generator level jet
struct GenJet : public Jet {

    static size_t iWgt, //!< global index for weight
                  iScl; //!< global index for scale

    float weight () const final; //!< uses `iWgt` internally
    float CorrPt  () const final; //!< uses `iScl` internally
    float CorrEta () const final; //!< uses `iScl` internally

    virtual GenJet operator+ (const GenJet&) const; //!< merges two jets (the removal of the original jets is left to the user, if applicable)

    void clear () override; //!< resets all members to the same values as in the constructor
    ~GenJet () = default;
};

////////////////////////////////////////////////////////////////////////////////
/// A detector level jet
struct RecJet : public Jet {

    static size_t iWgt, //!< global index for weight
                  iScl; //!< global index for scale

    float weight () const final; //!< uses `iWgt` internally
    float CorrPt  () const final; //!< uses `iScl` internally
    float CorrEta () const final; //!< uses `iScl` internally

    virtual RecJet operator+ (const RecJet&) const; //!< merges two jets (the removal of the original jets is left to the user, if applicable)

    float area; //!< jet area, roughly = pi*R^2
    std::vector<std::vector<float>> tags; //!< discriminants (should be known by the user)

    void clear () override; //!< resets all members to the same values as in the constructor
    RecJet ();
    ~RecJet () = default;
};

//////////////////////////////////////////////////////////////////////////////////
///// Any combination of two objects (e.g. a resonance decay or a dijet)
//template<typename First, typename Second> struct Comb {
//
//    First& first; //!< first decay product (typically with the highest energy)
//    Second& second; //!< second decay product
//
//    FourVector operator() () const //!< creates a 4-vec on the fly, using `iScl` internally
//    {
//        FourVector c1 = first(),
//                   c2 = second();
//        return c1 + c2;
//    }
//
//    void clear ()
//    {
//        first.clear();
//        second.clear();
//    }
//
//    inline float weight () //!< weight product of components, using `iWgt` internally
//    {
//        return first.weight() * second.weight();
//    }
//};

//template<typename Jet> Dijet : public Comb<Jet,Jet>, public FourVector {
//
//    Dijet (Jet& jet1, Jet& jet2) :
//        Comb<Jet,Jet>{jet1, jet2},
//        FourVector(Comb<Jet,Jet>::operator()()) 
//    { }
//    
//    inline float Ymax () { return max(first.AbsRap(), second.AbsRap()); }
//    inline float Dphi () { return DeltaPhi(first.Phi(), second.Phi()); }
//    inline float PtAve () { return Pt()/2; }
//
//};
//
//typedef Dijet<RecJet> RecDijet;
//typedef Dijet<GenJet> GenDijet;

//////////////////////////////////////////////////////////////////////////////////
///// Any combination of two objects after a fit
//template<typename First, typename Second> struct CombFit : public Comb<First,Second> {
//    bool valid; //!< flag for convergence
//    float chi2; //!< output from fit
//    int ndf; //!< number of degrees of freedom
//    // TODO: covariance, etc.?
//};
//
//typedef Comb<RecJet,RecJet> RecVecBosonHad;
//typedef Comb<GenJet,GenJet> GenVecBosonHad;
//
//typedef Comb<RecVecBosonHad,RecJet> RecTop;
//typedef Comb<RecVecBosonHad,GenJet> GenTop;
//
//typedef CombFit<RecVecBosonHad,RecJet> RecTTbarFullHad;
//typedef CombFit<GenVecBosonHad,GenJet> GenTTbarFullHad;

}

////////////////////////////////////////////////////////////////////////////////
/// Stream operator for event at generator level
std::ostream& operator<< (std::ostream&, const Darwin::Physics::GenEvent&);

////////////////////////////////////////////////////////////////////////////////
/// Stream operator for event at detector level
std::ostream& operator<< (std::ostream&, const Darwin::Physics::RecEvent&);

////////////////////////////////////////////////////////////////////////////////
/// Stream operator for FourVectors
std::ostream& operator<< (std::ostream&, const Darwin::Physics::FourVector&);

////////////////////////////////////////////////////////////////////////////////
/// Stream operator for jets
std::ostream& operator<< (std::ostream&, const Darwin::Physics::Jet&);

#endif
