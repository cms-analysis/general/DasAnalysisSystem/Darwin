#ifndef DARWIN_OPTIONS_H
#define DARWIN_OPTIONS_H

#include <functional>
#include <vector>
#include <filesystem>
#include <iostream>
#include <optional>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/positional_options.hpp>
#include <boost/property_tree/ptree.hpp>

#ifndef DARWIN_GIT_COMMIT
#  define DARWIN_GIT_COMMIT nullptr
#endif

namespace Darwin::Tools {

enum {
    none    = 0b00000, //!< default (for simple executables)
    config  = 0b00001, //!< activate `-c` option to provide config file
    split   = 0b00010, //!< activate `-k` and `-j` to define slice
    fill    = 0b00100, //!< activate `-f` to fill the tree
    syst    = 0b01000, //!< activate `-s` to systematic uncertainties
    verbose = 0b10000  //!< bit for debug mode (`-v` is always available)
}; //!< parameters (input) / steering (output) for program options

////////////////////////////////////////////////////////////////////////////////
/// \brief Common class to interpret the command line, based on Boost Program Options.
///
/// Options may also be partly provided via a configuration file.
/// Additional options to split the input file are also possible.
/// Minimal working example:
/// ~~~{.cpp}
/// Options options("Global description.", Options::config | Options::split);
/// options.input ("input" , &input , "input ROOT file" );
/// options.output("output", &output, "output ROOT file");
/// options.arg<int>("myArg", "correction.here", "my description");
/// options.args("allOtherArgs", "correction.there.list", "garbage collector");
/// auto const& config = options(argc, argv);
/// ~~~
/// where the config is a Boost Property Tree. A one-liner is even possible:
/// ~~~{.cpp}
/// auto const& config = Options("Global description.", Options::config | Options::split).input("input", &input, "input ROOT file").output("output", &output, "output ROOT file").arg<int>("myArg", "correction.here", "my description").args("allOtherArgs", "correction.there.list", "garbage collector")(argc, argv);
/// ~~~
/// The order matters only for the positional options, where input(s) should be
/// given first, then output(s), then additional positional options.
/// The position of explicit options (e.g. `-v`) does not matter. `Options::args`
/// may be used as a garbage collector for extra positional options.
class Options {

    // internal parsing
    boost::program_options::options_description hidden, //!< hidden interface (not for lambda user)
                                                helper, //!< to display the helper
                                                common, //!< generic + explicit options like `--verbose`
                                                custom; //!< for positional arguments, depending on the actual command
    boost::program_options::positional_options_description pos_hide; //!< parser for positional arguments
    boost::property_tree::ptree pt_conf; //!< internal config obtained from arugments and input config

    ////////////////////////////////////////////////////////////////////////////////
    /// First parser that is called. As soon as `-h` is given, or if the command
    /// is run without options, then the helper is shown. It should be directly given
    /// `argc` and `argv` from the main function.
    void parse_helper (int, const char * const []);

    ////////////////////////////////////////////////////////////////////////////////
    /// Parser for generic options, such as the config (with `-c`) or slices.
    /// It should be directly given `argc` and `argv` from the main function.
    void parse_common (int, const char * const []);

    ////////////////////////////////////////////////////////////////////////////////
    /// Parser for options provided with `args()`, defined differently in each
    /// application. It should be directly given `argc` and `argv` from the main
    /// function.
    void parse_custom (int, const char * const []);

    std::filesystem::path config_file; //!< path to INFO, JSON, or XML config file

    // for helper
    const std::string tutorial; //!< define in constructor, shown with option `-t`
    std::string synopsis; //!< stores the clean version of the command, displayed with `-h`
    std::vector<std::string> names, //!< names of the different options (shown in `synopsis`)
                             configpaths; //!< path to the options in config (except for I/O)

    ////////////////////////////////////////////////////////////////////////////////
    /// Function used by Boost Program Options to check if the file does exist, and
    /// if yes, if it is readable and readable.
    static void check_input (const std::filesystem::path& //!< path to input
                            );

    ////////////////////////////////////////////////////////////////////////////////
    /// Function used by Boost Program Options to check if the file may already
    /// exist, and if yes, then if it is writable, and not a directory.
    /// (At this stage, the automatic naming of the output according to the name
    /// of the input should not happen. Such a thing should rather be done in the
    /// executable directly.)
    static void check_output (const std::filesystem::path& //!< path to output
                             );

    ////////////////////////////////////////////////////////////////////////////////
    /// Generic code to add options. It is called internally by `Options::input()`,
    ///  `Options::output()`, and `Options::args()`.
    ///
    /// \return the object itself, so that the arguments can be given in a row.
    Options& set (const char *, //!< options name
            const boost::program_options::value_semantic *, //!< https://www.boost.org/doc/libs/1_80_0/doc/html/boost/program_options/value_semantic.html
            const char * //!< description (for helper)
            );

    ////////////////////////////////////////////////////////////////////////////////
    /// Helper to call `boost::property_tree::put` in `Darwin::Toos::Options::args`.
    ///
    /// \return `true` if `Options::registered` has been initialised.
    template<typename T //!< type of variable to store
        > std::function<void(T)> put (const char * configpath //!< path in config file
                                     )
    {
        return [configpath,this](T value) { pt_conf.put<T>(configpath, value); };
    }

    std::optional<unsigned> registered; //!< collect the number of registered options (only if `Options::args` has been called before)

    ////////////////////////////////////////////////////////////////////////////////
    /// \return `true` if `Options::registered` has been initialised.
    inline bool allow_unregistered () const { return bool(registered); }

    ////////////////////////////////////////////////////////////////////////////////
    /// \return output from command given to shell
    std::string exec (const std::string& //!< cmd to run in the shell
                );

    enum Stage {
        Input, //!< first the inputs
        Output, //!< then the outputs (inputs are no longer allowed)
        Arg, //!< then the registered arguments (inputs and outputs no longer allowed)
        Args //!< finally the remaining arguments / garbage collector (must be very last)
    }; //!< internal flag to ensure the right order of the option types

    Stage stage; //!< internal stage

    const int params; //!< input parameters to interpret explicit options
    const char* commit; //!< Commit SHA for --git
    int steer; //!< output parameters for code executation
    static std::filesystem::path prefix; //!< prefix command to steer `-j` and `-k`
    unsigned j, //!< # slices
             k; //!< slice index

public:

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor:
    /// - contains a parser for the help itself
    /// - contains a parser for the options, like config file and verbosity
    /// - and contains a parser for the input, output, and any other option
    ///   as positional arguments
    Options (const std::string&, //!< description, to be displayed in the helper
             int = none, //!< flags for explicit, generic command options (see enum)
             const char* commit = DARWIN_GIT_COMMIT //!< git commit for --git
             );

    ////////////////////////////////////////////////////////////////////////////////
    /// No default constructor is allowed
    Options () = delete;

    ////////////////////////////////////////////////////////////////////////////////
    /// Member to add an input. It can be called several times in a row.
    /// This option should always be provided from the command line, and never
    /// via the config file.
    ///
    /// \return the object itself, so that the arguments can be given in a row.
    Options& input  (const char *, //!< name of the option, will be shown in helper
                    std::filesystem::path *, //!< path pointer to file
                    const char * //!< description, shown in helper too
                    );

    ////////////////////////////////////////////////////////////////////////////////
    /// Member to add an undefined amount of input. It can be called only once.
    /// This option should always be provided from the command line, and never
    /// via the config file. There can be no garbage collector in addition.
    ///
    /// \return the object itself, so that the arguments can be given in a row.
    Options& inputs (const char *, //!< name of the options, will be shown in helper
                    std::vector<std::filesystem::path> *, //!< paths to files
                    const char * //!< descriptions 
                    );

    ////////////////////////////////////////////////////////////////////////////////
    /// Member to add an output. It can be called several times in a row.
    /// This option should always be provided from the command line, and never
    /// via the config file.
    ///
    /// \return the object itself, so that the arguments can be given in a row.
    Options& output (const char *, //!< name of the option, will be shown in helper
                    std::filesystem::path *, //!< path pointer to file
                    const char * //!< description, shown in helper too
                    );

    ////////////////////////////////////////////////////////////////////////////////
    /// Member to add an argument. It can be called several times in a row. Each
    /// argument may be provided via the configuration file too.
    ///
    /// \return the object itself, so that the arguments can be given in a row.
    template<typename T> Options& arg (const char * name, //!< name of the option, will be shown in helper
                                       const char * configpath, //!< path in config
                                       const char * desc //!< description, shown in helper too
            )
    {
        if (stage > Stage::Arg)
            BOOST_THROW_EXCEPTION(std::runtime_error("Not possible to add another argument at this stage"));
        stage = Stage::Arg;

        names.push_back(name);
        configpaths.push_back(configpath);
        const boost::program_options::value_semantic * s =
            boost::program_options::value<T>()->notifier(put<T>(configpath));
        return set(name, s, desc);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Member to collect any addition arguments provided in command line. It should
    /// be called maximum once. The additional options will be collected as a list
    /// of strings.
    Options& args (const char * name, //!< generic name for unregistered options
                   const char * configpath, //!< path in config.
                   const char * desc //!< description, shown in helper too
                   );

    ////////////////////////////////////////////////////////////////////////////////
    /// Unique parser accessible by the user of the class. It should be directly
    /// given `argc` and `argv` from the main function.
    const boost::property_tree::ptree& operator() (int, const char * const []);

    ////////////////////////////////////////////////////////////////////////////////
    /// Parse environment variable in string. Adapted from
    /// https://codereview.stackexchange.com/questions/172644/c-environment-variable-expansion
    ///
    /// \remark Curly brackets are needed (e.g. `${VAR}`, not just `$VAR`)
    static std::string parse_env_var (std::string);

    ////////////////////////////////////////////////////////////////////////////////
    /// Parse environment variable in C-style string.
    static inline const char * parse_env_var (const char * p)
    { return parse_env_var(std::string(p)).c_str(); }

    ////////////////////////////////////////////////////////////////////////////////
    /// Parse environment variable in a path.
    static inline std::filesystem::path parse_env_var (const std::filesystem::path& p)
    { return std::filesystem::path(parse_env_var(p.string())); }

    ////////////////////////////////////////////////////////////////////////////////
    /// Parse config if given. This method calls itself to parse the tree structure.
    static void parse_config (boost::property_tree::ptree&, //!< input config (given with `-c`)
                                std::string = "" //!< key in config (for recursive call)
                                );

    // result of parsing
    static std::string full_cmd; //!< extended version of the command for reproducibility

    ////////////////////////////////////////////////////////////////////////////////
    /// Steering information for running of executable
    inline int steering () const { return steer; }

    ////////////////////////////////////////////////////////////////////////////////
    /// Compactify slice information into a pair.
    inline std::pair<unsigned, unsigned> slice () const
    {
        if (!(params & split))
            BOOST_THROW_EXCEPTION(std::invalid_argument("No splitting of the input file."));
        return std::make_pair(j,k);
    }

    static const std::filesystem::path example; //!< path to example config
};

} // end of namespace

#endif
