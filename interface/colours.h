#ifndef DARWIN_COLOURS_H
#define DARWIN_COLOURS_H
[[ maybe_unused ]]
static const char * green     = "\x1B[32m",
                  * red       = "\x1B[31m",
                  * orange    = "\x1B[33m",
                  * bold      = "\x1B[1m",
                  * normal    = "\x1B[22m",
                  * highlight = "\x1B[3m", 
                  * underline = "\x1B[4m", 
                  * def       = "\x1B[0m";
#endif
