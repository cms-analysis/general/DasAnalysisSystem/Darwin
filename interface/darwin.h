#ifndef DARWIN_COMMON_H
#define DARWIN_COMMON_H

#include <string>
#include <vector>
#include <memory>
#include <filesystem>
#include <functional>
#include <system_error>
#include <cstdio>
#include <string>

#include "colours.h"
#include "exceptions.h"
#include "MetaInfo.h"
#include "Options.h"
#include "Looper.h"

#include <TROOT.h>
#include <TFile.h>
#include <TH1.h>
#include <TChain.h>
#include <TString.h>
#include <TKey.h>

#include <boost/exception/all.hpp>

namespace Darwin::Tools {

#ifndef DARWIN_GIT_REPO
#  ifndef DARWIN
#    error "Provide the location of the Git repository at compile time."
#  else
#    define DARWIN_GIT_REPO DARWIN
#  endif
#endif
const std::filesystem::path MetaInfo::origin = DARWIN_GIT_REPO;

#ifndef DARWIN_EXAMPLE
#  ifndef DARWIN
#    error "Provide the location of the example configuration at compile time."
#  endif
#  define DARWIN_EXAMPLE DARWIN "/test/example.info"
#endif
const std::filesystem::path Options::example = DARWIN_EXAMPLE;

////////////////////////////////////////////////////////////////////////////////
/// Shortcut to create a reproducible output file (see ROOT Doxygen for details)
inline std::unique_ptr<TFile> GetOutput (std::filesystem::path output,
                                         const char * name)
{
    namespace fs = std::filesystem;
    const auto fname = output;
    output += "?reproducible="; output += name;
    std::unique_ptr<TFile> f(TFile::Open(output.c_str(), "RECREATE"));
    if (!f || !f->IsOpen() || f->IsZombie() || !fs::exists(fname))
        BOOST_THROW_EXCEPTION( fs::filesystem_error("Failed to open output file",
                                output, make_error_code(std::errc::io_error)) );
    return f;
}

////////////////////////////////////////////////////////////////////////////////
/// Standard initialisation for ROOT.
///
/// "Now you own all histogram objects and you will need to delete them, for in-
/// stance through the use of `std::unique_ptr`. You can still set the directory 
/// of a histogram by calling `SetDirectory()` once it has been created."
inline void StandardInit ()
{
    gROOT->SetBatch();
    TH1::SetDefaultSumw2();
    TH1::AddDirectory(kFALSE);
}

////////////////////////////////////////////////////////////////////////////////
/// In case directories are given as inputs, instead of files, this function
/// looks for all ROOT files in those directories (and subdirectories, iteratively).
inline std::vector<std::filesystem::path> GetROOTfiles
        (const std::vector<std::filesystem::path>& inputs //!< ROOT files or directories
        )
{
    using namespace std;
    namespace fs = filesystem;

    if (inputs.size() == 0)
        BOOST_THROW_EXCEPTION(fs::filesystem_error("No input was found!",
                                make_error_code(errc::no_such_file_or_directory)));

    vector<fs::path> root_inputs;
    root_inputs.reserve(inputs.size());
    for (auto input: inputs) {
        input = fs::absolute(input);
        if (fs::is_directory(input)) {
            vector<fs::path> subinputs;
            subinputs.reserve(distance(fs::directory_iterator(input), {}));
            for (auto const& dir_entry: fs::directory_iterator{input})
                subinputs.push_back(dir_entry.path());
            subinputs = GetROOTfiles(subinputs);
            root_inputs.insert(end(root_inputs), begin(subinputs), end(subinputs));
        }
        else if (input.extension() == ".root")
            root_inputs.push_back(input);
    }

    return root_inputs;
}

////////////////////////////////////////////////////////////////////////////////
/// Load chain from a list of files.
inline std::unique_ptr<TChain> GetChain
                (std::vector<std::filesystem::path> inputs, //!< ROOT files or directories
                 const char * name = "events") //!< internal path to `TTree`
{
    using namespace std;
    namespace fs = filesystem;

    unique_ptr<TChain> chain = make_unique<TChain>(name);
    inputs = GetROOTfiles(inputs);
    for (auto const& input: inputs) {
        int code = chain->Add(input.c_str(), 0); // "If nentries<=0 and wildcarding is not used,
                                                 // returns 1 if the file exists and contains
                                                 // the correct tree and 0 otherwise"
        if (code == 1) continue; // i.e. the tree was found successfully
        auto fIn = make_unique<TFile>(input.c_str(), "READ");
        namespace DE = Darwin::Exceptions;
        BOOST_THROW_EXCEPTION(DE::BadInput("The tree cannot be "
                        "found in (one of) the input file(s).", fIn));
    }
    if (chain->GetEntries() == 0)
        BOOST_THROW_EXCEPTION(std::invalid_argument("Empty trees in input!"));
    return chain;
}

////////////////////////////////////////////////////////////////////////////////
/// Load a histogram from a list of files.
template<typename THX = TH1>
inline std::unique_ptr<THX> GetHist
                (std::vector<std::filesystem::path> inputs, //!< ROOT files or directories
                 const char * name = "h") //!< internal path to ROOT histogram
{
    using namespace std;
    namespace fs = filesystem;

    unique_ptr<THX> sum;
    inputs = GetROOTfiles(inputs);
    for (auto const& input: inputs) {
        auto fIn = make_unique<TFile>(input.c_str(), "READ");
        // TODO: what if the file had already been opened?? will it be closed prematurely?
        unique_ptr<THX> h(fIn->Get<THX>(name));
        if (!h) {
            namespace DE = Darwin::Exceptions;
            BOOST_THROW_EXCEPTION(DE::BadInput(Form("`%s` cannot be found in (one of) the "
                                                    " file(s).", name), fIn));
        }
        if (sum)
            sum->Add(h.get());
        else {
            sum = move(h);
            sum->SetDirectory(nullptr);
        }
    }
    return sum;
}

////////////////////////////////////////////////////////////////////////////////
/// Look for a tree in the input file (stop at the first found one).
///
/// NOTE: in principle, we take the risk of not looking at the same `TTree` as
///       all other executables, but this is unlikely in practice
inline std::string GetFirstTreeLocation
                (const std::filesystem::path& input //!< one input ROOT file
                )
{
    using namespace std;
    namespace fs = filesystem;
    namespace DE = Darwin::Exceptions;

    unique_ptr<TFile> fIn(TFile::Open(input.c_str(), "READ"));
    if (!fIn || !fIn->IsOpen() || fIn->IsZombie() || !fs::exists(input))
        BOOST_THROW_EXCEPTION( fs::filesystem_error("Failed to open input file",
                                input, make_error_code(std::errc::io_error)) );

    function<string(TDirectory *)> loop;
    loop = [&loop](TDirectory * d) -> string {

        // look for a `TTree` in the current `TDirectory`
        for (const auto&& obj: *(d->GetListOfKeys())) {
            auto const key = dynamic_cast<TKey*>(obj);
            if (dynamic_cast<TTree *>(key->ReadObj()) == nullptr) continue;
            return obj->GetName();
        }

        // if not found, check all sub`TDirectory`s
        for (const auto&& obj: *(d->GetListOfKeys())) {
            auto const key = dynamic_cast<TKey*>(obj);
            auto dd = dynamic_cast<TDirectory *>(key->ReadObj());
            if (dd == nullptr) continue;
            if (auto location = loop(dd); !location.empty())
                return Form("%s/%s", dd->GetName(), location.c_str());
        }

        // if nothing was found at this stage, return empty string
        return string();
    };
    string location = loop(fIn.get());
    if (location.empty())
        BOOST_THROW_EXCEPTION( DE::BadInput("No `TTree` could be found in the input file.", fIn) );
    return location;
}

}

#define DT_GetOutput(output) Darwin::Tools::GetOutput(output, __func__)

#endif
