#!/usr/bin/env python3

from argparse import ArgumentParser
from pathlib import Path
from typing import List
from subprocess import check_output
from shutil import copy2, which
from glob import glob
import multiprocessing
import os, sys, textwrap

__all__ = ["PrefixCommand", "preparse", "tweak_helper_multi", "diagnostic", "git_hash"]

NPROC = multiprocessing.cpu_count()


def preparse(argv: List[str], tutorial: str, multi_opt: bool = False, dag_opt: bool = False, condor: bool = False):
    """Parser for the prefix command itself, relying on ArgumentParser."""

    parser = ArgumentParser(add_help=False)

    parser.add_argument("exec", nargs="?")
    if multi_opt:
        parser.add_argument(
            "-b", "--background", action="store_true"
        )
        parser.add_argument(
            "-d", "--dry-run", action="store_true"
        )
    if dag_opt:
        parser.add_argument(
            "-d", "--dag", type=Path
        )
    if condor:
        parser.add_argument(
            "-n", "--memory-needs", type=int, default=1024
        )

    # helper
    parser.add_argument("-h", "--help", action="store_true")
    parser.add_argument("-t", "--tutorial", action="store_true")
    parser.add_argument("-g", "--git", action="store_true")
    if not dag_opt:
        parser.add_argument("-e", "--example", action="store_true")

    # common
    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument("-m", "--mute", action="store_true")
    parser.add_argument("-f", "--fill", action="store_true")
    parser.add_argument("-s", "--syst", action="store_true")
    parser.add_argument("-c", "--config", type=Path)
    parser.add_argument("-j", "--nSplit", type=int, default=NPROC)

    # custom will be in args
    cmds, args = parser.parse_known_intermixed_args(argv[1:])

    if any(x in args for x in ["-k", "--nNow"]):
        raise ValueError("Giving `-k` or `--nNow` is meaningless in this context.")
    if any(map(lambda arg: arg[0] == "-", args)):
        raise ValueError("No unknown explicit option is allowed.")

    # restore explicit options
    if cmds.verbose:
        args += ["-v"]
    if cmds.mute:
        args += ["-m"]
    if cmds.fill:
        args += ["-f"]
    if cmds.syst:
        args += ["-s"]
    if cmds.config:
        args += ["-c", str(cmds.config.absolute())]

    if not cmds.exec:
        if cmds.help or len(argv) == 1:
            prefix = os.path.basename(sys.argv[0])
            print(f"\33[1m{prefix} exec args [...]\33[0m",
                   "where\texec = a command using `Darwin::Tools::Options` with `Darwin::Tools::split`",
                   "\targs = the arguments of the same command, one being called exactly `output`",
                  sep='\n')
        if cmds.tutorial:
            # boost::program_options::options_description::m_default_line_length = 80
            lines = textwrap.TextWrapper(width=80).wrap(text=tutorial)
            print(*lines, sep='\n')
        parser.exit()

    return cmds, args


def can_parallelize(helper) -> bool:
    """Checks if the command can be parallelized."""
    nSplit_exists = ["nSplit" in row for row in helper].count(True)
    output_exists = ["output" in row for row in helper].count(True)
    return nSplit_exists and output_exists


def git_hash(exec: str) -> None:
    """Returns the SHA of the executable at compile time."""
    print(check_output(exec + ' -g', shell=True).decode(), end='')


class PrefixCommand:
    """General utilities for prefix commands (try, parallel, submit).
    The explicit options (`cmds`) are unchanged; among the implicit options,
    the inputs are checked (if any), and the output is adapted to each thread."""

    name: str = ""
    cmds: List[str] = []
    inputs: List[str] = []
    absinputs: List[str] = []
    output: str = ""
    absoutput: Path = ""
    args: List[str] = []
    helper: str = ""

    def __init__(self, prefix: str, cmds: List[str]) -> None:
        """Constructor from a separated list of arguments."""

        self.name = os.path.basename(prefix)
        self.cmds = cmds

        self.helper = self._help_text()
        if not can_parallelize(self.helper[1:]):
            raise ValueError(
                f"`{self.cmds.exec}`"
                + " does not match the requirements to be run with this prefix command: "
                + "exactly one `output` argument and `nSplit` argument must be defined."
            )

    def _help_text(self) -> str:
        """Returns the command's normal help text."""
        # if executable is given, then the helper is adapted to match with the prefix command
        text = check_output(self.cmds.exec + ' -h', shell=True).decode().strip().split("\n")
        return text

    def _synopsis(self) -> List[str]:
        """Extract synopsis from helper (after undoing the bold characters)."""
        synopsis = self.helper[0].replace("\x1b[1m", "").replace("\x1b[0m", "")
        return synopsis.split()[1:]

    def parse(self, args: List[str]) -> None:
        """Put input(s), output, and other arguments in dedicated members."""

        synopsis = self._synopsis()
        i = synopsis.index("output")
        if i >= len(args):
            raise ValueError("Unable to identify `output` in given arguments")

        self.inputs = args[:i]
        self.output = args[i]
        if len(args) > i:
            self.args = args[i + 1 :]

    def get_entries(self, input) -> int:
        """Obtain the number of entries from input n-tuple."""

        nevents = int(
            check_output(["printEntries"] + [ input.strip("\\\"") ])
            .decode()
            .splitlines()[0]
            .replace("\n", "")
        )
        if nevents == 0:
            raise ValueError("Empty input tree.")
        return nevents

    def prepare_single(self) -> None:
        """Prepare the working area for a single-run command."""

        self.absoutput = Path(self.output).absolute()

        if self.absoutput.exists():
            if self.absoutput.is_dir():
                self.absoutput /= "0.root"
            if self.absoutput.exists() and not os.access(self.absoutput, os.W_OK):
                raise ValueError(
                    self.absoutput + " is not writable"
                )
        else:
            if len(self.absoutput.suffix) == 0: # then assume it's a directory
                self.absoutput.mkdir(parents=True)
                self.absoutput /= "0.root"

    def prepare_multi(self) -> None:
        """Prepare the members for parallel running (local or on farm)."""

        for x in self.inputs:
            self.absinputs += glob(x)
        if len(self.inputs) > 0 and len(self.absinputs) == 0:
            raise ValueError("Inputs could not be found: " + ' '.join(self.inputs))
        self.absinputs = [str(Path(x).absolute()) for x in self.absinputs]

        self.inputs = ['"' + x + '"' for x in self.inputs]
        self.absoutput = Path(self.output).absolute()
        if self.absoutput.suffix != '':
            raise ValueError(f"A directory with an extension is misleading: {self.output}")

        if self.absoutput.exists():
            if self.absoutput.is_file():
                raise ValueError(self.output + " is not a directory")
            for k in range(self.cmds.nSplit):
                rootfile = self.absoutput / f"{k}.root"
                if rootfile.exists() and not os.access(rootfile, os.W_OK):
                    raise ValueError(
                        "The root files in " + self.output + " are not writable"
                    )
            rootfile = self.absoutput / f"{self.cmds.nSplit}.root"
            if rootfile.exists():
                raise ValueError(
                    "The output directory contains supernumerary root files from a former run."
                )

        else:
            self.absoutput.mkdir(parents=True)

    def prepare_fire_and_forget(self) -> None:
        """Copies the executable, libraries, and dictionaries to the output directory."""

        # copy executables and libraries to working directory
        exec = Path(which(self.cmds.exec)).absolute()
        copy_exec = self.absoutput / exec.name
        if exec != copy_exec:
            copy2(exec, self.output)
            exec = copy_exec
        self.cmds.exec = str(exec)

        for lib in glob(os.environ['DARWIN_FIRE_AND_FORGET'] + "/*so"):
            copy2(lib, self.output)
        for dic in glob(os.environ['DARWIN_FIRE_AND_FORGET'] + "/*pcm"):
            copy2(dic, self.output)


def tweak_helper_multi(prefix: PrefixCommand,
        multi_opt: bool = True, dag_opt: bool = False, condor: bool = False) -> None:
    """Tweak helper to use a directory as output rather than a file
    and to reflect the number of cores of the machine."""

    print(f"\33[1m{prefix.name} \33[0m", end="")
    for row in prefix.helper:
        if "output" in row:
            row = row.replace("ROOT file", "directory")
        if "nNow" in row:
            continue
        if "nSplit" in row:
            row = row.replace(
                "(=1)" if NPROC < 10 else "(=1) ", "(=" + str(NPROC) + ")"
            )
        print(row)
        if "Helper" in row:
            if multi_opt:
                print("  -b [ --background ]       Do not wait for job to finish")
                print("  -d [ --dry-run ]          Run until the actual executation")
            if dag_opt:
                print("  -d [ --dag ]              Indicate DAGMan directory location")
            if condor:
                print("  -n [ --memory-needs] arg  Memory needs for HTCondor job")
    print('')


def diagnostic(exception: Exception) -> None:
    """Common diagnostic message for all prefix commands in case of exception."""
    prefix = os.path.basename(sys.argv[0])
    versions = check_output("printDarwinSoftwareVersion", shell=True).decode().strip().split("\n")
    versions += [f"Python {sys.version_info.major}.{sys.version_info.minor}"]
    print(f"\x1B[31m{prefix}: \x1B[1m{exception}\x1B[22m", ", ".join(versions),
           "Consider opening a GitLab issue if you don't understand the cause of the failure.\x1B[0m",
          sep='\n')
