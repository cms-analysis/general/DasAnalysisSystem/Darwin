#include <cstdlib>
#include <ctime>

#include <iostream>
#include <iomanip>
#include <limits>

#include "colours.h"
#include "MetaInfo.h"

#include <TTree.h>
#include <TROOT.h>

#include <git2.h>

#ifndef DARWIN_GIT_COMMIT
# include "version.h"  // Used by the CMake build system
#endif

using namespace std;
namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace Darwin::Exceptions {

struct : public error_category {
    const char * name () const throw() { return "git error"; }
    string message (int ev) const { return "error code returned by libgit2: " + to_string(ev); }
} git_error_category; //!< Error category for libgit2
}

using namespace Darwin::Tools;
using namespace Darwin::Exceptions;

int MetaInfo::get_one_status (const char * p, unsigned int status, void * data)
{
    auto repo = static_cast<git_repository *>(data);
    int ignore = 0;
    // https://libgit2.org/libgit2/#HEAD/group/ignore/git_ignore_path_is_ignored
    int git_error = git_ignore_path_is_ignored(&ignore, repo, p);
    if (git_error != 0)
        BOOST_THROW_EXCEPTION(fs::filesystem_error("File status could not be determined", p, error_code{git_error, git_error_category}));
    return (ignore == 1) ? EXIT_SUCCESS : EXIT_FAILURE;
}

void MetaInfo::GitInit ()
{
    // https://libgit2.org/libgit2/#HEAD/group/libgit2/git_libgit2_init
    git_libgit2_init();

    if (origin.empty())
        BOOST_THROW_EXCEPTION(fs::filesystem_error("Empty path to Git repository", origin, make_error_code(errc::invalid_argument)));

    // open repo
    if (!(fs::exists(origin) && fs::is_directory(origin)))
        BOOST_THROW_EXCEPTION(fs::filesystem_error("Git repository cannot be found", origin, make_error_code(errc::not_a_directory)));

    // https://libgit2.org/libgit2/#HEAD/group/repository/git_repository_open
    git_error = git_repository_open(&repo, origin.c_str());
    if (git_error != 0)
        BOOST_THROW_EXCEPTION(fs::filesystem_error("Invalid git repository", origin, error_code{git_error, git_error_category})); 

    // get HEAD
    git_object * head_commit = nullptr;
    // https://libgit2.org/libgit2/#HEAD/group/revparse/git_revparse_single
    git_error = git_revparse_single(&head_commit, repo, "HEAD^{commit}");
    if (git_error != 0)
        BOOST_THROW_EXCEPTION(system_error(git_error, git_error_category, "Head commit could not be determined "
                    "(check if $DARWIN_GIT_REPO has been defined and points to the root of the git repository)"));
    auto commit = reinterpret_cast<git_commit *>(head_commit); 

    // get SHA-1
    const git_oid * id = git_commit_id(commit);
    // https://libgit2.org/libgit2/#HEAD/group/oid/git_oid_tostr
    git_oid_tostr(sha, 16, id);

    // display a few properties
    // https://libgit2.org/libgit2/#HEAD/group/commit/git_commit_author
    const git_signature * author = git_commit_author(commit);
    // https://libgit2.org/libgit2/#HEAD/group/commit/git_commit_message
    cout << underline << "latest commit" << def << ": " << git_commit_message(commit)
         << "               by " << author->name << " (" << sha << ")" << endl;

    // https://libgit2.org/libgit2/#HEAD/group/commit/git_commit_free
    git_commit_free(commit);
}

void MetaInfo::PrintStatus () const
{
    git_status_list * status = nullptr;
    // https://libgit2.org/libgit2/#HEAD/group/status/git_status_list_new
    git_error = git_status_list_new(&status, repo, nullptr);
    if (git_error != 0)
        BOOST_THROW_EXCEPTION(system_error(git_error, git_error_category, "File status could not be determined"));
    // https://libgit2.org/libgit2/#HEAD/group/status/git_status_list_entrycount
    const size_t maxi = git_status_list_entrycount(status);

    for (size_t i = 0; i < maxi; ++i) {
        const git_status_entry * s = git_status_byindex(status, i);

        if (s->status == GIT_STATUS_CURRENT) continue;
        if (s->status & GIT_STATUS_IGNORED) continue;

        char istatus = ' ', // index status
             wstatus = ' '; // worktree status
        const char * extra = "", // for submodule info
                   * a = nullptr,
                   * b = nullptr,
                   * c = nullptr;

             if (s->status & GIT_STATUS_INDEX_NEW       ) istatus = 'A';
        else if (s->status & GIT_STATUS_INDEX_MODIFIED  ) istatus = 'M';
        else if (s->status & GIT_STATUS_INDEX_DELETED   ) istatus = 'D';
        else if (s->status & GIT_STATUS_INDEX_RENAMED   ) istatus = 'R';
        else if (s->status & GIT_STATUS_INDEX_TYPECHANGE) istatus = 'T';

        if (s->status & GIT_STATUS_WT_NEW) {
            if (istatus == ' ') {                         istatus = '?'; }
                                                       wstatus = '?';
        }
        else if (s->status & GIT_STATUS_WT_MODIFIED  ) wstatus = 'M';
        else if (s->status & GIT_STATUS_WT_DELETED   ) wstatus = 'D';
        else if (s->status & GIT_STATUS_WT_RENAMED   ) wstatus = 'R';
        else if (s->status & GIT_STATUS_WT_TYPECHANGE) wstatus = 'T';
        if (istatus == '?' && wstatus == '?') continue; // TODO?

        // A commit in a tree is how submodules are stored, so let's go take a look at its status.
        if (s->index_to_workdir &&
                s->index_to_workdir->new_file.mode == GIT_FILEMODE_COMMIT) {
            unsigned int smstatus = 0;

            // https://libgit2.org/libgit2/#HEAD/group/submodule/git_submodule_status
            if (!git_submodule_status(&smstatus, repo, s->index_to_workdir->new_file.path,
                        GIT_SUBMODULE_IGNORE_UNSPECIFIED)) {
                if      (smstatus & GIT_SUBMODULE_STATUS_WD_MODIFIED      ) extra = " (new commits)";
                else if (smstatus & GIT_SUBMODULE_STATUS_WD_INDEX_MODIFIED) extra = " (modified content)";
                else if (smstatus & GIT_SUBMODULE_STATUS_WD_WD_MODIFIED   ) extra = " (modified content)";
                else if (smstatus & GIT_SUBMODULE_STATUS_WD_UNTRACKED     ) extra = " (untracked content)";
            }
        }

        // Now that we have all the information, format the output.
        if (s->head_to_index) {
            a = s->head_to_index->old_file.path;
            b = s->head_to_index->new_file.path;
        }
        if (s->index_to_workdir) {
            if (!a) a = s->index_to_workdir->old_file.path;
            if (!b) b = s->index_to_workdir->old_file.path;
            c = s->index_to_workdir->new_file.path;
        }

        cout << istatus << wstatus << ' ' << a;
        if (istatus == 'R') cout << ' ' << b;
        if (wstatus == 'R') cout << ' ' << c;
        cout << extra << endl;
    }

    for (size_t i = 0; i < maxi; ++i) {
        // https://libgit2.org/libgit2/#HEAD/group/status/git_status_byindex
        const git_status_entry * s = git_status_byindex(status, i);
        if (s->status != GIT_STATUS_WT_NEW) continue;
        cout << "?? " << s->index_to_workdir->old_file.path << endl;
    }
    // https://libgit2.org/libgit2/#HEAD/group/status/git_status_list_free
    git_status_list_free(status);
}

bool MetaInfo::AllChangesCommitted () const
{
    // https://libgit2.org/libgit2/#HEAD/group/status/git_status_foreach
    return git_status_foreach(repo, get_one_status, repo) == 0;
}

void MetaInfo::UpdateList (const bool assumeComplete) const
{
    if (Empty())
        BOOST_THROW_EXCEPTION(invalid_argument("No metainfo in input to update."));

    if (!Find("flags"))
        BOOST_THROW_EXCEPTION(invalid_argument("Missing flags in input."));
    if (!Find("git"))
        BOOST_THROW_EXCEPTION(invalid_argument("Missing git information in input."));

    /****** git business ******/

    if (origin != Get<fs::path>("git", "repo")) {
        cerr << orange << "Warning: inconsistent repositories between input and output.\n" << def;
        Set<fs::path>("git", "repo", origin);
    }
    else if (sha != Get<string>("git", "commit")) {
        cerr << orange << "Warning: inconsistent commits between input and output.\n" << def;
        Set<string>("git", "commit", string(sha));
    }

    bool all_changes_committed = AllChangesCommitted();
    if (!all_changes_committed) {
        cerr << orange << "Warning: uncommited changes.\n";
        PrintStatus();
        cerr << def;
    }
    auto reproducible = Get<bool>("git", "reproducible");
    if (!reproducible)
        cerr << orange << "Warning: input tree not reproducible.\n" << def;
    Set<bool>("git", "reproducible", all_changes_committed && reproducible);

    auto complete = Get<bool>("git", "complete");
    if (!complete)
        cerr << orange << "Warning: input tree not complete.\n" << def;
    Set<bool>("git", "complete", assumeComplete && complete);

    /****** software business ******/

    for (auto const& version: versions) {
        string const& soft = version.first,
                      vers1 = version.second;
        if (!Find("software", soft.c_str())) continue;
        auto vers2 = Get<string>("software", soft.c_str());

        if (vers1 == vers2) continue;
        cerr << orange << "Warning: inconsistent " << soft << " versions ("
             << vers1 << " vs " << vers2 << ")\n" << def;
    }

    /****** seeding business ******/

    if (!Find("preseed"))
        BOOST_THROW_EXCEPTION(invalid_argument("No preseed could be found."));
}

std::string MetaInfo::libgit2_version ()
{
    int major, minor, rev;
    // https://libgit2.org/libgit2/#HEAD/group/libgit2/git_libgit2_version
    int git_error = git_libgit2_version(&major, &minor, &rev);
    if (git_error != 0)
        BOOST_THROW_EXCEPTION(runtime_error("Unable to get libgit2 version"));
    return Form("%d.%d.%d", major, minor, rev);
}

std::map<std::string, std::string> MetaInfo::versions = {
    {"Darwin" , DARWIN_VERSION},
    {"gpp"    , Form("%d.%d.%d", __GNUC__, __GNUC_MINOR__, __GNUC_PATCHLEVEL__)},
    {"Cpp"    , Form("C++%ld", __cplusplus/100-2000)},
    {"ROOT"   , gROOT->GetVersion()},
    {"Boost"  , BOOST_LIB_VERSION},
    {"libgit2", libgit2_version()}
};

MetaInfo::MetaInfo (TTree * t,
                    const boost::property_tree::ptree& config,
                    bool git_init)
    : UserInfo(t), repo(nullptr)
{
    if (!Empty())
        BOOST_THROW_EXCEPTION(invalid_argument("Only a new tree's metainfo may be initialised."));

    try {
        // flags
        const auto& flags = config.get_child("flags");
        Set<bool>("flags", "isMC", flags.get<bool>("isMC"));
        Set<int >("flags", "year", flags.get<int >("year"));
        Set<int >("flags", "R"   , flags.get<int >("R"   ));
        const auto& labels = flags.get_child_optional("labels");
        if (labels) // note: here, we use `boost::optional`,
                    // not `std::optional`, hence we have to test explicitly
        for (const auto& label: *labels) {
            // there are subtle differences between INFO, JSON, and XML
            // - in INFO: the label is stored in the key and the value is empty
            // - in JSON: the label is stored in the value and the key is empty
            // - in XML: the label is stored in the value and the key is "item"
            string key = label.first,
                   value = label.second.get_value<string>();

            if (key.empty() || key == "item") 
                Set<string>("flags", "labels", value);
            else if (value.empty())
                Set<string>("flags", "labels", key);
            else
                BOOST_THROW_EXCEPTION(invalid_argument("Empty labels are not allowed"));
        }

        if (config.count("history"))
            Set<string>("history", config.get<string>("history"));

        if (config.count("preseed"))
            Set<int>("preseed", config.get<int>("preseed"));
        else {
            int preseed = time(nullptr) % numeric_limits<int>::max();
            Set<int>("preseed", preseed);
        }
    }
    catch (const pt::ptree_error& e) {
        BOOST_THROW_EXCEPTION(e);
    }

    // git
    bool all_changes_committed = false;
    if (git_init) {
        GitInit();
        all_changes_committed = AllChangesCommitted();
        if (!all_changes_committed) {
            cerr << orange << "Warning: uncommited changes.\n";
            PrintStatus();
            cerr << def;
        }
        Set<string>("git", "commit", string(sha));
    }
    else {
        Set<string>("git", "commit", config.get<string>("git.commit"));
        all_changes_committed = config.get<bool>("git.reproducible");
    }
    Set<fs::path>("git", "repo"    , origin               );
    Set<bool>("git", "reproducible", all_changes_committed);
    Set<bool>("git", "complete"    , false                );

    for (auto const& version: versions) {
        string const& soft = version.first,
                      vers = version.second;
        Set<string>("software", soft.c_str(), vers);
    }
}

MetaInfo::MetaInfo (TTree * t, bool git_init)
    : UserInfo(t), repo(nullptr)
{ 
    if (!git_init) return;
    GitInit();
    UpdateList(false);
}

MetaInfo::MetaInfo (TList * l, bool git_init)
    : UserInfo(l), repo(nullptr)
{
    if (!git_init) return;
    GitInit();
    UpdateList(true);
}

MetaInfo::~MetaInfo ()
{
    if (repo == nullptr) return;
    git_repository_free(repo);
    git_libgit2_shutdown();
}

void MetaInfo::Check (const boost::property_tree::ptree& config) const
{
    try {
        const auto& flags = config.get_child_optional("flags");
        if (flags) {
            if (Get<bool>("flags", "isMC") != flags->get<bool>("isMC") ||
                Get<int >("flags", "year") != flags->get<int >("year") ||
                Get<int >("flags", "R"   ) != flags->get<int >("R"   ))
                BOOST_THROW_EXCEPTION(invalid_argument("Inconsistent flags between metainfo and config"));

            const auto& labels_in_config = flags->get_child_optional("labels");
            if (labels_in_config) // TODO?
            for (const auto& label: *labels_in_config) {
                string key = label.first,
                       value = label.second.get_value<string>();

                if (key.empty() || key == "item") {
                    if (Find("flags", "labels", value)) continue;
                    cerr << orange << "Warning: Adding label '" + value + "' in config, which was not found in input metainfo.\n" << def;
                    Set<string>("flags", "labels", value);
                }
                else if (value.empty()) {
                    if (Find("flags", "labels", key)) continue;
                    cerr << orange << "Warning: Adding label '" + key + "' in config, which was not found in input metainfo.\n" << def;
                    Set<string>("flags", "labels", key);
                }
            }
            if (Find("flags", "labels")) {
                const auto& labels_in_metainfo = MkPtree(List("flags", "labels"));
                for (const auto& label: labels_in_metainfo) {
                    string value = label.second.get_value<string>();
                    if (labels_in_config->count(value)) continue;
                    bool found = false;
                    if (labels_in_config)
                    for (const auto& label2: *labels_in_config) {
                        string value2 = label2.second.get_value<string>();
                        found = found || (value2 == value);
                    }
                    if (!found)
                        cerr << orange << "Warning: Label '" + value + "' in metainfo was not found in config.\n" << def;
                }
            }
        }

        // in case the config comes from `getMetaInfo`, this checks if anything has changed meanwhile
        const auto& git = config.get_child_optional("git");
        if (git) {
            auto commit = git->get<string>("commit");
            if (origin != Get<string>("git", "repo"))
                cerr << orange << "Warning: inconsistent repositories between metainfo and config.\n" << def;
            else if (origin == Get<string>("git", "repo") && sha != Get<string>("git", "commit"))
                cerr << orange << "Warning: inconsistent commits between metainfo and config.\n" << def;
            else if (!git->get<bool>("reproducible") || !git->get<bool>("complete"))
                cerr << orange << "Warning: the input config was generated from an irreproducible or incomplete file.\n" << def;
            else if (AllChangesCommitted() && Get<bool>("git", "reproducible") && Get<bool>("git", "complete"))
                cout << green << "Expecting to reproduce the same result as the one used to generate the input config!" << def << endl;
        }

        if (config.count("history"))
            Set<string>("history", config.get<string>("history"));

        if (config.count("preseed")) {
            auto c = config.get<int>("preseed"),
                 s = Get<int>("preseed");
            if (c != s)
                cerr << orange << "The preseed in the sample (" << s << ") differs from the preseed in the config (" << c << "). The value from the config will be " << bold << "ignored" << normal << ".\n" << def;
        }
    }
    catch (const pt::ptree_error& e) {
        BOOST_THROW_EXCEPTION(e);
    }
}
