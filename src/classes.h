#ifndef DARWIN_CLASSES_H
#define DARWIN_CLASSES_H

#include "Objects.h"

Darwin::Physics::GenEvent Darwin__genevent;
Darwin::Physics::RecEvent Darwin__recevent;
Darwin::Physics::Trigger Darwin__trigger;
//Darwin::Physics::genMET Darwin__genmet;
Darwin::Physics::RecMET Darwin__recmet;
Darwin::Physics::PileUp Darwin__pileup;
Darwin::Physics::Vertex Darwin__vertex;

std::vector<Darwin::Physics::Trigger> Darwin__triggers;

Darwin::Physics::FourVector Darwin__p4;
Darwin::Physics::GenJet Darwin__genjet;
Darwin::Physics::RecJet Darwin__recjet;

std::vector<Darwin::Physics::FourVector> Darwin__p4s;
std::vector<Darwin::Physics::GenJet> Darwin__genjets;
std::vector<Darwin::Physics::RecJet> Darwin__recjets;

#endif
