#ifndef DOXYGEN_SHOULD_SKIP_THIS
#define DOXYGEN_SHOULD_SKIP_THIS
#define BOOST_TEST_MODULE test
#include <boost/test/included/unit_test.hpp>
#include <boost/exception/all.hpp>

#include <boost/property_tree/ptree.hpp>      
#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include "test.h"
#include "exceptions.h"
#include "Options.h"
#include "UserInfo.h"
#include "MetaInfo.h"

#ifndef DARWIN
#error "DARWIN (containing the absolute path to the root of the repo) must be given to gcc with option `-D`"
#endif
#define DARWIN_GIT_REPO DARWIN

#include "example01.cc"
#include "example02.cc"
#include "example03.cc"
#include "../bin/getMetaInfo.cc"
#include "../bin/printMetaInfo.cc"
#include "../bin/printEntries.cc"
#include "../bin/printBranches.cc"
#include "../bin/forceMetaInfo.cc"

using namespace std;

namespace DT = Darwin::Tools;
using DT::tokenize;

namespace Darwin::Physics {

template<bool DARWIN_TEST_EXCEPTIONS = false>
void example02 (const fs::path& input, const fs::path& output,
                const pt::ptree& config, const int steering,
                const DT::Slice slice = {1,0} )
{
    example02<DARWIN_TEST_EXCEPTIONS>(vector<fs::path>{input}, output, config, steering, slice);
}

void example03 (const fs::path& input, const fs::path& output,
                const pt::ptree& config, const int steering,
                const DT::Slice slice = {1,0})
{
    example03(vector<fs::path>{input}, output, config, steering, slice);
}

}
namespace DP = Darwin::Physics;
namespace DE = Darwin::Exceptions;

BOOST_AUTO_TEST_SUITE( test_hadd_like_fcts )

    BOOST_AUTO_TEST_CASE( output )
    {
        BOOST_REQUIRE_THROW( DT_GetOutput("/this/file/does/not/exists.root"), boost::wrapexcept<fs::filesystem_error> );
        BOOST_REQUIRE_NO_THROW( DT_GetOutput("this_file_is_allowed.root") );
    }

    BOOST_AUTO_TEST_CASE( getters )
    {
        vector<fs::path> files;
        BOOST_REQUIRE_THROW( DT::GetHist<TH1>(files, "h"), boost::wrapexcept<fs::filesystem_error> ); // bc empty
        BOOST_REQUIRE_THROW( DT::GetChain    (files, "t"), boost::wrapexcept<fs::filesystem_error> ); // bc empty

        for (int i = 1; i < 4; ++i) {
            fs::path name = Form("getters_%d.root", i);
            cout << name << endl;
            files.push_back(name);
            unique_ptr<TFile> f(TFile::Open(name.c_str(), "RECREATE"));
            auto h = make_unique<TH1F>("h", "h", 1, 0., 1.);
            auto t = make_unique<TTree>("t", "t");
            h->SetDirectory(f.get());
            t->SetDirectory(f.get());
            h->Write();
            t->Write();
        }

        BOOST_REQUIRE_THROW( DT::GetChain    (files, "t"                   ), boost::wrapexcept<invalid_argument> );
        BOOST_REQUIRE_THROW( DT::GetHist<TH1>(files, "thisHistDoesNotExist"), boost::wrapexcept<DE::BadInput> );
        BOOST_REQUIRE_THROW( DT::GetChain    (files, "thisTreeDoesNotExist"), boost::wrapexcept<DE::BadInput> );

        BOOST_REQUIRE_NO_THROW( DT::GetHist<TH1>(files, "h") );
    }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE( test_examples )

    BOOST_AUTO_TEST_CASE( ntuple_producer )
    {
        fs::path output;

        DT::Options options("This is just a toy example to generate a n-tuple.", DT::config | DT::fill);
        options.output("output"    , &output, "output ROOT file")
               .arg<bool>  ("isMC" , "flags.isMC" , "flag"                                   )
               .arg<float> ("R"    , "flags.R"    , "R parameter in jet clustering algorithm")
               .arg<int  > ("year" , "flags.year" , "year (20xx)"                            );

        auto const args = tokenize("example01 ntuple01.root -c example.info -f");
        BOOST_REQUIRE_NO_THROW( options(args.size(), args.data()) );

        const auto& config = options(args.size(), args.data());
        const int steering = options.steering();

        BOOST_REQUIRE_NO_THROW( DP::example01(output, config, steering) );
    }

    BOOST_AUTO_TEST_CASE( ntuple_modifier )
    {
        fs::path input, output;

        DT::Options options("This is just a toy to play with the options and with the exceptions.",
                            DT::config | DT::split | DT::fill | DT::syst);
        options.input ("input" , &input , "input ROOT file" )
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("hotzones"  , "corrections.hotzones"  , "location of hot zones in the calorimeter")
               .arg<fs::path>("METfilters", "corrections.METfilters", "location of list of MET filters to apply");

        auto const args = tokenize("example02 ntuple01.root ntuple02.root -c example.info -j 2 -k 1 -f -s");
        options(args.size(), args.data());
        BOOST_REQUIRE_NO_THROW( options(args.size(), args.data()) );

        const auto& config = options(args.size(), args.data());
        const auto& slice = options.slice();
        const int steering = options.steering();

        DP::example02(input, output, config, steering, slice);
        BOOST_REQUIRE_NO_THROW( DP::example02(input, output, config, steering, slice) );

        fs::remove("ntuple01.root");
    }

    BOOST_AUTO_TEST_CASE( projection )
    {
        fs::path input, output;

        DT::Options options("Toy example to project a n-tuple.", DT::split);
        options.input ("input" , &input , "input ROOT file" )
               .output("output", &output, "output ROOT file");

        auto const args = tokenize("example03 ntuple02.root hists03.root");
        BOOST_REQUIRE_NO_THROW( options(args.size(), args.data()) );

        const auto& config = options(args.size(), args.data());
        const auto& slice = options.slice();
        const int steering = options.steering();

        BOOST_REQUIRE_NO_THROW( DP::example03(input, output, config, steering, slice) );

        fs::remove("ntuple02.root");
        fs::remove("hists03.root");
    }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE( whole_cycle )

    BOOST_AUTO_TEST_CASE( all_steps )
    {
        pt::ptree config;
        BOOST_REQUIRE_NO_THROW( pt::read_info("example.info", config) );
        BOOST_REQUIRE_NO_THROW( DT::Options::parse_config(config) );
        const int steering = DT::fill | DT::syst;
        BOOST_REQUIRE_NO_THROW( DP::example01("ntuple01.root", config, steering) );
        BOOST_REQUIRE_NO_THROW( DP::example02("ntuple01.root", "ntuple02.root", config, steering) );
        BOOST_REQUIRE_NO_THROW( DP::example03("ntuple02.root", "hists03.root", config, steering) );
    }

    BOOST_AUTO_TEST_CASE( getBool )
    {
        BOOST_TEST( !DT::getBool("this will not work").has_value());
        BOOST_TEST( DT::getBool("yes").has_value());
        BOOST_TEST( DT::getBool("true" ).value() == true );
        BOOST_TEST( DT::getBool("kTRUE").value() == true );
        BOOST_TEST( DT::getBool("off"  ).value() == false );
        BOOST_TEST( DT::getBool("NO"   ).value() == false );
    }

    BOOST_AUTO_TEST_CASE( forceMetaInfo )
    {
        vector<fs::path> input = {"ntuple02.root"};
        DT::getMetaInfo(input, "force02.info");
        DT::forceMetaInfo("force02.info", "ntuple01.root", DT::verbose);
        input = {"ntuple01.root"};
        DT::getMetaInfo(input, "force01.info");
        unique_ptr<TFile> f(TFile::Open("ntuple01.root", "READ"));
        auto events = unique_ptr<TTree>(f->Get<TTree>("events"));
        DT::MetaInfo metainfo(events);
        BOOST_TEST( !metainfo.Find("history") );
        BOOST_TEST( !metainfo.Get<bool>("git", "reproducible") );
    }

    BOOST_AUTO_TEST_CASE( GetFirstTreeLocation )
    {
        BOOST_TEST( DT::GetFirstTreeLocation("ntuple01.root") == "events" );
        BOOST_REQUIRE_NO_THROW( DT::GetFirstTreeLocation("ntuple01.root") );

        TFile::Open("GetFirstTreeLocation.root", "RECREATE")->Close();
        BOOST_REQUIRE_THROW( DT::GetFirstTreeLocation("GetFirstTreeLocation.root"), boost::wrapexcept<DE::BadInput> );
    }

    BOOST_AUTO_TEST_CASE( printEntries )
    {
        auto f = TFile::Open("printEntries.root", "RECREATE");
        f->mkdir("dir1");
        auto d = f->mkdir("dir2"); d->cd();
        auto dd = d->mkdir("subdir"); dd->cd();
        auto t = new TTree("one_tree", "");
        t->SetDirectory(dd);
        t->Write();
        f->Close();
        BOOST_REQUIRE_NO_THROW( DT::printEntries({"printEntries.root"}) );
        fs::remove("printEntries.root");
    }

    BOOST_AUTO_TEST_CASE( printBranches )
    {
        vector<fs::path> inputs {"ntuple01.root"};
        BOOST_REQUIRE_NO_THROW( DT::printBranches(inputs) );
    }

    BOOST_AUTO_TEST_CASE( printMetaInfo )
    {
        vector<fs::path> inputs {"ntuple01.root"};
        pt::ptree tree;
        tree.put<string>("path", "preseed");
        BOOST_REQUIRE_NO_THROW( DT::printMetaInfo(inputs, tree) );
    }

    BOOST_AUTO_TEST_CASE( closure )
    {
        {
            unique_ptr<TFile> f(TFile::Open("ntuple02.root"));
            f->ls();
        }

        vector<string> exts {"info", "xml", "json"};
        for (string ext: exts) {
            fs::path file = "out1." + ext;
            vector<fs::path> inputs = {"ntuple02.root"};
            BOOST_REQUIRE_NO_THROW( DT::getMetaInfo(inputs, file) );
        }

        auto getHash = [](const fs::path& ntuple) {
            auto rootfile = unique_ptr<TFile>(TFile::Open(ntuple.c_str(), "READ"));
            auto events = unique_ptr<TTree>(rootfile->Get<TTree>("events"));
            size_t Hash = hash<TTree*>{}(events.get());
            return Hash;
        };

        size_t Hash = getHash("ntuple02.root");

        pt::ptree config;
        auto chain = [&config,&getHash](const fs::path& fOut, bool isMC) {
            fs::remove("ntuple02.root");
            config.put<bool>("flags.isMC", isMC);
            const int steering = DT::fill | DT::syst;
            BOOST_REQUIRE_NO_THROW( DT::Options::parse_config(config) );
            DP::example01("ntuple01.root", config, steering);
            DP::example02("ntuple01.root", "ntuple02.root", config, steering);
            DP::example03("ntuple02.root", "hists03.root", config, steering);
            vector<fs::path> inputs = {"hists03.root"};
            DT::getMetaInfo(inputs, fOut);
            size_t Hash = getHash("ntuple02.root");
            fs::remove("ntuple01.root");
            fs::rename("hists03.root", Form("hists03_%s_%s.root", &(fOut.extension().c_str()[1]), isMC ? "true" : "false"));
            return Hash;
        };

        pt::read_info("out1.info", config);
        int oldseed = config.get<int>("preseed");
        config.put<int>("preseed", 2*oldseed);
        BOOST_TEST( Hash != chain("out2.info", true ) );
        config.put<int>("preseed", oldseed);
        BOOST_TEST( Hash == chain("out2.info", true ) );
        BOOST_TEST( Hash != chain("out2.info", false) );

        pt::read_json("out1.json", config);
        BOOST_TEST( Hash == chain("out2.json", true ) );
        BOOST_TEST( Hash != chain("out2.json", false) );

        pt::read_xml("out1.xml", config);
        config = config.get_child("userinfo");
        BOOST_TEST( Hash == chain("out2.xml", true ) );
        BOOST_TEST( Hash != chain("out2.xml", false) );

        vector<fs::path> inputs = {"hists03_info_true.root" , "hists03_json_true.root" , "hists03_xml_true.root" };
        BOOST_TEST( DT::getMetaInfo(inputs, "true.info"   ) == EXIT_SUCCESS );
        inputs = {"hists03_info_false.root", "hists03_json_false.root", "hists03_xml_false.root"};
        BOOST_TEST( DT::getMetaInfo(inputs, "false.info"  ) == EXIT_SUCCESS );
        inputs = {"hists03_info_true.root" , "hists03_info_false.root"};
        BOOST_TEST( DT::getMetaInfo(inputs, "failure.info") == EXIT_FAILURE );
        
        for (string name: {"true", "false", "failure"})
            fs::remove(name + ".info");

        for (string ext: exts) {
            fs::remove("out1." + ext);
            fs::remove("out2." + ext);
            for (string b: {"true", "false"})
                fs::remove("hists03_" + ext + "_" + b + ".root");
        }
    }

    BOOST_AUTO_TEST_CASE( exceptions )
    {
        // testing raw exception object
        auto f = unique_ptr<TFile>(TFile::Open("ntuple02.root", "READ"));
        auto t = unique_ptr<TTree>(f->Get<TTree>("events"));
        DT::MetaInfo metainfo(t);
        cout << DE::BadInput("Here is a bad input", metainfo).what() << endl;
        t->GetEntry(5);
        cout << DE::AnomalousEvent("Here is an anomally (just for the example)", t).what() << endl;

        // testing in real context
        pt::ptree config;
        BOOST_REQUIRE_NO_THROW( pt::read_info(DARWIN "/test/example.info", config) );
        BOOST_REQUIRE_NO_THROW( DT::Options::parse_config(config) );
        const int steering = DT::fill | DT::syst;
        DP::example01("ntuple01.root", config, steering);
        BOOST_REQUIRE_THROW( DP::example02<true>("ntuple01.root",  "ntuple02.root", config, steering), boost::wrapexcept<DE::AnomalousEvent> );
        BOOST_REQUIRE_THROW( DP::example01<true>("ntuple01.root",                   config, steering), boost::wrapexcept<DE::BadInput      > );

        fs::remove("inexistent.out");
        fs::remove("ntuple01.root");
    }

BOOST_AUTO_TEST_SUITE_END()

#endif
