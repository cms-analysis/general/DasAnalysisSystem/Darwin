#ifndef DOXYGEN_SHOULD_SKIP_THIS
#define BOOST_TEST_MODULE testOptions

#include <optional>

#include <boost/test/included/unit_test.hpp>
#include <boost/exception/all.hpp>

#include "darwin.h"
#include "test.h"
#include "Options.h"

#include <TFile.h>

using namespace std;
namespace fs = std::filesystem;

using namespace Darwin::Tools;

namespace po = boost::program_options;
namespace pt = boost::property_tree;

BOOST_AUTO_TEST_SUITE( test_options )

    BOOST_AUTO_TEST_CASE( test_default )
    {
        BOOST_REQUIRE_NO_THROW( Options("Test") );

        Options options("Test"); // no config, no splitting by default
        BOOST_TEST( options.full_cmd.empty() );

        TFile::Open("input.root", "RECREATE")->Close();

        fs::path input, output;
        BOOST_REQUIRE_NO_THROW( options.input("input", &input, "input ROOT file") );
        BOOST_REQUIRE_NO_THROW( options.output("output", &output, "output ROOT file") );

        {
            // testing normal commands
            auto const args = tokenize("exec input.root output.root --unknown");
            BOOST_REQUIRE_THROW( options(args.size(), args.data()), boost::wrapexcept<po::error> );
        }

        {
            // testing normal commands
            auto const args = tokenize("exec input.root output.root");
            BOOST_REQUIRE_NO_THROW( options(args.size(), args.data()) );
            BOOST_TEST( input == "input.root" );
            BOOST_TEST( output == "output.root" );
            cout << options.full_cmd << endl;
            BOOST_TEST( !options.full_cmd.empty() );
        }

        fs::remove("output.root");

        {
            // testing bad input
            auto const args = tokenize("exec inpu.root output.root");
            BOOST_REQUIRE_THROW( options(args.size(),args.data()), boost::wrapexcept<fs::filesystem_error> );
        }

        {
            // testing bad output
            auto const args = tokenize("exec input.root .");
            BOOST_REQUIRE_THROW( options(args.size(),args.data()), boost::wrapexcept<fs::filesystem_error> );
        }

        {
            // testing non-readable input
            fs::permissions("input.root", fs::perms::owner_read, fs::perm_options::remove);
            auto const args = tokenize("exec input.root output.root");
            BOOST_REQUIRE_THROW( options(args.size(),args.data()), boost::wrapexcept<fs::filesystem_error> );
            fs::permissions("input.root", fs::perms::owner_read, fs::perm_options::add);
        }

        TFile::Open("output.root", "RECREATE")->Close();

        {
            // testing non-writable output
            fs::permissions("output.root", fs::perms::owner_write, fs::perm_options::remove);
            auto const args = tokenize("exec input.root output.root");
            BOOST_REQUIRE_THROW( options(args.size(),args.data()), boost::wrapexcept<fs::filesystem_error> );
            fs::permissions("output.root", fs::perms::owner_write, fs::perm_options::add);
        }

        fs::remove("input.root");
        fs::remove("output.root");
    }

    BOOST_AUTO_TEST_CASE( test_example_config )
    {
        Options options("Test", config);
        options.arg<fs::path>("blah", "foo.bar", "this must fail bc this path does not exist in the example config");
        auto const args = tokenize("exec -e");
        BOOST_REQUIRE_THROW( options(args.size(), args.data()), boost::wrapexcept<pt::ptree_bad_path> );
    }

    BOOST_AUTO_TEST_CASE( test_splitting )
    {
        BOOST_REQUIRE_NO_THROW( Options("Test", split) );
        Options options("Test", split);

        TFile::Open("input.root", "RECREATE")->Close();

        fs::path input, output;
        BOOST_REQUIRE_NO_THROW( options.input("input", &input, "input ROOT file") );
        BOOST_REQUIRE_NO_THROW( options.output("output", &output, "output ROOT file") );

        // testing normal commands
        auto const args = tokenize("exec input.root output.root -j 42 -k 24");
        BOOST_REQUIRE_NO_THROW( options(args.size(), args.data()) );
        auto const slice = options.slice();
        BOOST_TEST( slice.first  == 42u );
        BOOST_TEST( slice.second == 24u );

        fs::remove("input.root");
        fs::remove("output.root");
    }

    BOOST_AUTO_TEST_CASE( test_parse_env_var )
    {
        BOOST_REQUIRE_NO_THROW( Options::parse_env_var("${HOME}") );
        BOOST_REQUIRE_THROW( Options::parse_env_var("${THISVARDOESNOTEXIST}"), boost::wrapexcept<std::runtime_error> );
    }

    BOOST_AUTO_TEST_CASE( test_config )
    {
        BOOST_REQUIRE_NO_THROW( Options("Test", config) );
        Options options("Test", config);

        TFile::Open("input.root", "RECREATE")->Close();

        fs::path input, output;
        BOOST_REQUIRE_NO_THROW( options.input("input", &input, "input ROOT file") );
        BOOST_REQUIRE_NO_THROW( options.output("output", &output, "output ROOT file") );

        // testing normal commands
        auto const args = tokenize("exec input.root output.root -c " DARWIN_EXAMPLE);
        BOOST_TEST_REQUIRE( Options::parse_env_var("${DARWIN_TABLES}") ); // is used in `example.info`
        BOOST_REQUIRE_NO_THROW( options(args.size(), args.data()) );
        auto const& config = options(args.size(), args.data());
        BOOST_TEST( !config.empty() );
        for (auto it: config)
            cout << it.first << endl;
        BOOST_TEST( config.count("flags") );
        BOOST_TEST( config.count("corrections") );

        auto const flags = config.get_child("flags");
        BOOST_TEST( flags.count("isMC") );
        BOOST_TEST( flags.count("year") );
        BOOST_TEST( flags.count("R") );

        auto const corrections = config.get_child("corrections");
        BOOST_TEST( corrections.count("METfilters") );
        BOOST_TEST( corrections.count("hotzones") );
        BOOST_TEST( corrections.count("lumi") );
        BOOST_TEST( corrections.count("xsecs") );
        BOOST_TEST( corrections.count("JES") );
        BOOST_TEST( corrections.count("triggers") );
        BOOST_TEST( corrections.count("PUstaub") );

        fs::remove("input.root");
        fs::remove("output.root");
    }

    BOOST_AUTO_TEST_CASE( test_args )
    {
        Options options("Test");

        TFile::Open("input.root", "RECREATE")->Close();

        fs::path input, output;
        options.input("input", &input, "input") 
               .args("labels", "labels", "labels");
        BOOST_REQUIRE_THROW( options.output("output", &output, "output"), runtime_error );

        fs::remove("input.root");
    }

    BOOST_AUTO_TEST_CASE( test_stages )
    {
        Options options("Test");

        TFile::Open("input.root", "RECREATE")->Close();
        fs::path output, input;
        options.output("output", &output, "output");
        BOOST_REQUIRE_NO_THROW( options.arg<int>("R", "R", "R") );
        BOOST_REQUIRE_THROW( options.input("input", &input, "input"), runtime_error );
        fs::remove("input.root");
    }

    BOOST_AUTO_TEST_CASE( multiple_inputs )
    {
        for (int i = 0; i < 3; ++i)
            TFile::Open(Form("input%d.root", i), "RECREATE")->Close();

        {
            Options options("Test");
            vector<fs::path> inputs;
            BOOST_REQUIRE_NO_THROW( options.inputs("inputs", &inputs, "input ROOT files") );

            system("ls input*.root");
            auto const args = tokenize("exec input*.root");
            BOOST_REQUIRE_NO_THROW( options(args.size(), args.data()) );
            BOOST_TEST( inputs.size() == 3ul );
            for (auto const& input: inputs) 
                cout << input << ' ';
            cout << endl;
        }

        {
            Options options("Test");
            vector<fs::path> inputs;
            fs::path output;
            BOOST_REQUIRE_NO_THROW( options.inputs("inputs", &inputs, "input ROOT files") );
            options.output("output", &output, "output ROOT file");

            auto const args = tokenize("exec input*.root output.root");
            BOOST_REQUIRE_NO_THROW( options(args.size(), args.data()) );
            BOOST_TEST( inputs.size() == 3ul );
            for (auto const& input: inputs) 
                cout << input << ' ';
            cout << endl;
            BOOST_TEST( output == "output.root" );
        }

        for (int i = 0; i < 3; ++i)
            fs::remove(Form("input%d.root", i));
        fs::remove("output.root");

        {
            Options options("Test");
            vector<fs::path> inputs;
            options.inputs("inputs", &inputs, "input ROOT files");
            auto const args = tokenize("exec input*.root"); // no such file exists
            BOOST_REQUIRE_THROW( options(args.size(), args.data()), boost::wrapexcept<fs::filesystem_error> );
        }
    }

BOOST_AUTO_TEST_SUITE_END()

#endif
