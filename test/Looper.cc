#ifndef DOXYGEN_SHOULD_SKIP_THIS
#define DOXYGEN_SHOULD_SKIP_THIS
#define BOOST_TEST_MODULE testLooper
#include <boost/test/included/unit_test.hpp>
#include <boost/exception/all.hpp>

#include <TTree.h>
#include "Looper.h"

using namespace std;
using namespace Darwin::Tools;

BOOST_AUTO_TEST_SUITE( looper )

    BOOST_AUTO_TEST_CASE( shared )
    {
        auto t = make_unique<TTree>("events", "events");

        // call empty tree
        BOOST_REQUIRE_THROW( Looper(t, Slice{1,0}), boost::wrapexcept<invalid_argument> );

        // normal call (filling 100 events in two steps)
        for (Looper looper(100ll, {2,0}); looper(); ++looper) t->Fill();
        for (Looper looper(100ll, {2,1}); looper(); ++looper) t->Fill();

        // normal call
        BOOST_REQUIRE_NO_THROW( Looper(t, {10,5}) ); // 10 slices, take here the fifth, covering [50,60[

        // wrong order (5 slices, 10th slice)
        BOOST_REQUIRE_THROW( Looper(t, {5,10}), boost::wrapexcept<invalid_argument> );

        // too many slices (1000 > 100)
        BOOST_REQUIRE_THROW( Looper(t, {1000,0}), boost::wrapexcept<invalid_argument> );

        {
            Looper looper(t, {10,5});
            BOOST_TEST( *looper == 50 );
            BOOST_TEST( looper() );
            ++looper;
            BOOST_TEST( *looper == 51 );
        }

        long long i = 50;
        for (Looper looper(t, {10,5}); looper(); ++looper) {
            BOOST_TEST( *looper == i );
            ++i;
        }
    }

    BOOST_AUTO_TEST_CASE( unique )
    {
        // Test constructor with a unique_ptr argument
        auto t = make_unique<TTree>("events", "events");
        BOOST_REQUIRE_THROW( Looper(t, Slice{1,0}), boost::wrapexcept<invalid_argument> );
    }

BOOST_AUTO_TEST_SUITE_END()

#endif
