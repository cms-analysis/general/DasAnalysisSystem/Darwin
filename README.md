# Introduction {#mainpage}

![master](https://gitlab.cern.ch/Proto/Darwin/badges/master/pipeline.svg "CI")

## Principles

Make physics from the shell, factorise the analysis, and re-use existing tools as much as possible.

The input and output ROOT files should always be given explicitly to the command line; however, all parameters may be given either directly from the command (more concrete approach, good for teaching or for systematic studies) or from a configuration file (more abstract, good for reproducibility purposes). All along the processing, the history of the modification of the n-tuple is stored in the tree. This includes the values of the parameters, the series of command, a few flags, some git information, etc. This information may be used to generate a configuration file to reproduce the same n-tuple or to compare ROOT files with one another (using `getMetaInfo`).

A frequent mistake while developing an analysis framework is to design the code to run fast only once it is ready; this approach neglects the significant (and by far superior) amount of time spent at developing and debugging the framework. The present framework is intended to ease the development by factouring out each step of the analysis and sticking at a few healthy principles, such as not overengineering, testing while developing, etc., reducing primarily the debugging and development time, and only secondarily the running time. In practice, although we do not primarily optimise the total running time of the program itself, the running time spent by analysts may be significantly reduced.

The here provided libraries and tools try to leave enough freedom for innnovation. The framework is only loosely constraining: it is always possible to break the rules, as long as this does not break the existing tests. Of course, breaking the rule should (a priori) only be done for a valid reason, and by default, one should rather follow the present conventions.

## Technology

This framework is essentially written in C++17 and heavily relies on [ROOT](https://root.cern/) and [Boost](https://www.boost.org/). In a lower extent, [libgit2](https://libgit2.org/libgit2/#HEAD) is also used. Among the Boost libraries, several are used:
 - Property Tree (to handle the configuration files and the history of the file);
 - Program Options (the command line options);
 - Algorithms (string operations);
 - Test (unit tests),
 - Exceptions (exception handling).
