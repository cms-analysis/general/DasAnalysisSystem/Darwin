#include <cstdlib>
#include <iostream>
#include <filesystem>
#include <functional>

#include "darwin.h"

using namespace std;

namespace Darwin::Tools {

////////////////////////////////////////////////////////////////////////////////
/// Print the versions of the various pieces of software. Each software appears
/// on a new line. Tought for exception handling in Python code and for Doxygen.
void printDarwinSoftwareVersion ()
{
    // TODO: use accumulate
    for (auto const& v: MetaInfo::versions)
        cout << v.first << ' ' << v.second << '\n';
    cout << flush;
}

}

namespace DT = Darwin::Tools;
namespace DE = Darwin::Exceptions;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::Options options("Print the software versions.");

        DT::printDarwinSoftwareVersion();
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
