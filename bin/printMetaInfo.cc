#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <sstream>
#include <filesystem>
#include <memory>

#include "darwin.h"

#include <TFile.h>
#include <TTree.h>
#include <TString.h>
#include <TKey.h>

using namespace std;

namespace fs = filesystem;
namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;

namespace Darwin::Tools {

////////////////////////////////////////////////////////////////////////////////
/// Print the value(s) corresponding to a given key in the first given input
/// ROOT file.
///
/// If no value / subtree is found, the command fails and exits. If multiple
/// entries are found, they are all printed. Only the first subtree is displayed.
///
/// The INFO format allows a key to have a value *and* a child. Since the JSON
/// format does not allow that, we assume here that this case does not happen.
int printMetaInfo (vector<fs::path> inputs, //!< input ROOT files
                   const pt::ptree& config //!< config, with `flags` and `corrections` at least
                  )
{
    inputs = GetROOTfiles(inputs);
    const string location = GetFirstTreeLocation(inputs.front());

    // opening first file
    unique_ptr<TFile> fIn(TFile::Open(inputs.front().c_str(), "READ"));
    if (fIn->IsZombie())
        BOOST_THROW_EXCEPTION(DE::BadInput("Can't open the file.", fIn));

    // fetch tree
    auto t = shared_ptr<TTree>(dynamic_cast<TTree*>(fIn->Get<TTree>(location.c_str())));
    if (!t)
        BOOST_THROW_EXCEPTION(DE::BadInput("The tree can't be found in this file.", fIn));

    // open metainfo
    MetaInfo metainfo(t, false);
    auto ptree = metainfo.MkPtree();
    auto key = config.get<string>("path");

    auto value = ptree.get<string>(key);
    if (!value.empty()) {
        cout << value << endl;
        return EXIT_SUCCESS;
    }

    auto child = ptree.get_child_optional(key);
    if (child) {
        for (auto& el: *child) {
            string key = el.first,
                   val = el.second.get_value<string>();
            if (!key.empty()) cout << key << ' ';
            if (!val.empty()) cout << val;
            cout << '\n';
        }
        cout << flush;
        return EXIT_SUCCESS;
    }

    return EXIT_FAILURE;
}

}

namespace DT = Darwin::Tools;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;

        DT::Options options("Returns the value or direct subtree corresponding "
                            "to a given key.");
        options.inputs("inputs", &inputs, "input ROOT files or directory")
               .arg<string>("path", "path", "dot-separated full path to key");
        const auto& config = options(argc, argv);

        return DT::printMetaInfo(inputs, config);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }
}
#endif
