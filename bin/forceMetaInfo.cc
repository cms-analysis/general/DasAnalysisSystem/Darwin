#include <cstdlib>
#include <iostream>
#include <filesystem>
#include <memory>
#include <regex>

#include "darwin.h"

#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <boost/algorithm/string.hpp>

#include <TFile.h>
#include <TTree.h>
#include <TString.h>
#include <TKey.h>

using namespace std;

namespace fs = filesystem;
namespace pt = boost::property_tree;
namespace al = boost::algorithm;

namespace DE = Darwin::Exceptions;

namespace Darwin::Tools {

////////////////////////////////////////////////////////////////////////////////
/// Translates a string into a boolean if possible.
optional<bool> getBool (string s)
{
    al::to_lower(s);
    al::trim(s);
    static const array<string,4> trueStr {"true", "on", "ktrue", "yes"},
                                 falseStr {"false", "off", "kfalse", "no"};
    if (auto it = find(trueStr.begin(), trueStr.end(), s);
            it != trueStr.end())
        return make_optional<bool>(true);
    if (auto it = find(falseStr.begin(), falseStr.end(), s);
            it != falseStr.end())
        return make_optional<bool>(false);
    return {};
}

////////////////////////////////////////////////////////////////////////////////
/// Sets whatever is in the input config file into the metainfo of a `TTree`.
template<typename ... Args>
void FromPT2MetaInfo (const pt::ptree& elements, //!< from config file
                      const MetaInfo& metainfo, //!< target metainfo (will be modified)
                      ostream& cout, //!< overrides standard output
                      Args... args //!< key chain
                     )
{
    if constexpr (sizeof...(Args) < 10) // max depth
    for (const auto& element: elements) {
        const char * key = element.first.c_str();
        const auto& value = element.second.get_value<string>();

        cout << bold;
        if constexpr (sizeof...(Args) > 0)
            (cout << ... << args) << ' ';
        cout << def << key << " \"" << value << '"' << endl;

        FromPT2MetaInfo(element.second, metainfo, cout, args..., key);

        auto match = [&value](const string& reg) {
            return regex_match( value, regex(reg) );
        };
        if (auto isBool = getBool(value); isBool)
            metainfo.Set<bool>(args..., key, *isBool);
        else if (match("-?[0-9]+")) // integer
            metainfo.Set<int>(args..., key, stoi(value));
        else if (match("-?[0-9]+([\\.][0-9]+)?")) // floating number
            metainfo.Set<float>(args..., key, stof(value));
        else if (element.second.size() == 0) { // string
            if (value.empty()) {
                if constexpr (sizeof...(Args) > 0)
                    metainfo.Set<string>(args..., string(key));
            }
            else
                metainfo.Set<string>(args..., key, value);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
/// Force content of all `TTree::UserInfo()` and  `TList` found in the
/// topmost directory of the root file. This method is to be used as a last
/// resort, and certainly not in the context of reproducible results.
void forceMetaInfo (const fs::path& input, //!< input config file (INFO, JSON, XML)
                    const fs::path& output, //!< ROOT file
                    const int steering //!< parameters obtained from explicit options 
                   )
{
    pt::ptree config;
    if (input.extension() == ".json")
        pt::read_json(input, config);
    else if (input.extension() == ".xml")
        pt::read_xml(input, config);
    else
        pt::read_info(input, config);

    const string location = GetFirstTreeLocation(output);
    unique_ptr<TFile> fOut(TFile::Open(output.c_str(), "UPDATE"));
    auto t = shared_ptr<TTree>(fOut->Get<TTree>(location.c_str()));
    if (!t)
        BOOST_THROW_EXCEPTION(DE::BadInput("The tree can't be found in this file.", fOut));

    t->GetUserInfo()->Clear();

    static auto& cout = (steering & verbose) == verbose ? ::cout : dev_null;
    MetaInfo metainfo(t, false);
    FromPT2MetaInfo(config, metainfo, cout);
    metainfo.Set<bool>("git", "reproducible", false);
    if (metainfo.Find("history")) {
        TList * history = metainfo.List("history");
        metainfo.List()->Remove(history);
    }
    t->Write();
}

} // end of Darwin::Tools namespace

namespace DT = Darwin::Tools;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        fs::path input, output;

        DT::Options options("Force content of the first `TTree::UserInfo()` found any "
                            "directory of the output ROOT file. Use at your own risks.");
        options.input("input", &input, "input file (INFO, JSON, or XML)")
               .input("output", &output, "ROOT file");
        options(argc, argv);
        const int steering = options.steering();

        cerr << red << "Severe warning: you are running a dangerous command. "
                       "We hope that you know what you're doing.\n" << def;

        DT::forceMetaInfo(input, output, steering);

        return EXIT_SUCCESS;
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }
}
#endif
